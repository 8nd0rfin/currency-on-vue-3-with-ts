/**
 * Класс для работы по протоколу HTTP
 * Для данной задачи выбран интерфейс API XMLHttpRequest. И вынесен из контекста NUXT.
 * Можно использовать fetch, axios.
 * Также можно реализовать через vuex dispatch.
 */
import {getType} from "@/assets/js/utils.js";
const {getCurrencyPairs,getExchangeRates} = require('@/assets/js/Mock.ts')

/**
 * Конфигурация точек подключения (endpoints)
 * @type {{baseUrl: string, key: string}}
 */
const api = {
    baseUrl:'/',
    key:'SoMeRanDomApiKeyForStagingVersionOrProduction'
}

export class Http {
    http:XMLHttpRequest
    baseUrl:String
    apiKey:String
    defaultResponse:Object
    constructor() {
        this.http = new XMLHttpRequest()
        this.baseUrl = api.baseUrl
        this.apiKey = api.key
        this.defaultResponse = {success:false,data:'Запрос не отправлен',status:400,reason:'Запрос не отправлен'}
        if(!Object.prototype.hasOwnProperty.call(window, 'XMLHttpRequest')) {
            console.error('Api.class:constructor XMLHttpRequest не определён')
            return
        }
    }
    /**
     * Преобразует ответ запроса в необходимый формат.
     * В данном случае JSON
     * @param responseStr {String}
     * @returns {{success: *, statusText: (*|string|string), authenticatedOperation: boolean}|*[]|*}
     */
    async parseHttpResponse(responseStr:any) {
        const response = await responseStr.response
        if(getType(response) !== 'String') {
            console.error(`Api.class:parseHttpResponse response ожидается строка. Текущее значение ${response}`)
            return this.defaultResponse
        }
        if(typeof JSON === 'undefined') {
            console.error(`Api.class:parseHttpResponse библиотека JSON не определена`)
            return this.defaultResponse
        }
        if(response.indexOf('{') !== 0) {
            console.error(`Api.class:parseHttpResponse ответ от сервера не является валидным JSON объектом`)
            return this.defaultResponse
        }
        let parsed = null
        try {
            parsed = JSON.parse(response)
        } catch(e) {
            console.error(`Api.class:parseHttpResponse JSON.parse ошибка ${e}`)
            console.error(`Ответ от сервера ${response}`)
        }
        const success = response.success || false
        const status = response.status || 400
        const reason = response.statusText || response.reason

        if (status === 200) {
            return parsed
        }
        if (status >= 400 && status < 500) {
            return {success,status,data:'Api.class:parseHttpResponse ошибка сервера',reason}
        }
        if (status >= 500 && status < 600) {
            console.error(`Api.class:parseHttpResponse ошибка на стороне сервера. Status - ${status}. Response text - ${reason}. Response data - ${JSON.stringify(parsed)}.`)
            return {success,data:'Api.class:parseHttpResponse ошибка на стороне сервера',status,reason}
        }
        return {
            success,
            data:parsed,
            status,
            reason
        }
    }
    /**
     * Возвращает объект с допустимыми/необходимыми заголовками для запроса
     * @return {{"X-TIMESTAMP": number, "X-API-KEY", "Content-Type": (string)}}
     */
    getHeaders() {
        return {
            "Content-Type": "application/json",
            "X-Api-Key": this.apiKey,
            "X-Timestamp": (Date.now() / 1000).toString(),
        }
    }
    /**
     * Осуществляет запрос по протоколу HTTP
     * @param path
     * @param data
     * @param method
     * @return {Promise<Response<any>>}
     */
    request({path = '', query = {}, method = 'POST'}) {
        // Удалить при обращении к реальным API
        return new Promise((res) => {
            setTimeout(() => {
                if(path === 'getCurrencyPairs') {
                    res(getCurrencyPairs())
                }
                if(path === 'getExchangeRates'){
                    res(getExchangeRates())
                }
            },500)
        })
        // Использовать для отправки запросов к API
        /*
        if(this.http) {
            const _this = this
            const http = this.http
            const headers = this.getHeaders()
            return new Promise((res,rej) => {
                http.open(method, this.baseUrl + path, true)
                http.onload = async function() {
                    res(await _this.parseHttpResponse(http))
                }
                http.onprogress = function(event) {}
                http.onerror = function(e) {
                    console.error(`Api.class:request вызвал ошибку: ${e}`)
                    res(e)
                };
                http.onabort = function() {
                    console.error(`Api.class:request отменён`)
                    rej(_this.defaultResponse)
                };
                // Устанавливаем заголовки
                Object.keys(headers).forEach(key => {
                    console.log('---I---',key)
                    // http.setRequestHeader(key,headers[key])
                })
                http.send(JSON.stringify(query))
            })
        }
        return this.defaultResponse
         */
    }
}
