declare global {
    interface Window {
        $http?: any;
    }
}
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'

// Подключаем класс для отправки запросов через HTTP
const {Http} = require('@/assets/Api/Http.class.ts')
if(typeof window !== 'undefined') {
    window.$http = new Http()
} else {
    console.error('main.ts window is undefined')
}

const pinia = createPinia()
const app = createApp(App)
app.use(pinia)
app.mount('#currency')
