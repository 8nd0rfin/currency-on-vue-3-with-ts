import { CurrencyPair,base_currency,defaultCurrencyPair,defaultPairRate } from '@/assets/js/Mock'
import { defineStore } from 'pinia'
import { ref } from "vue"

export const CurrencyStore = defineStore('Currency', () => {
    // Метка процесса конвертации. True - только что была конвертация. Необходимо для отброса ложных срабатываний в watch`ерах
    const hasConverted = ref<boolean>(false)
    const convertationCommission = ref<string>('0')
    const from = ref<string>(base_currency)
    const to = ref<string>('USD')
    const pairs = ref()
    async function getPairs() {
        pairs.value = await window.$http.request({path: 'getCurrencyPairs'})
        return pairs.value
    }
    const rates = ref()
    async function getRates() {
        rates.value = await window.$http.request({path: 'getExchangeRates'})
        return rates.value
    }
    const setFrom = (value:string) => {
        from.value = value
    }
    const setTo = (value:string) => {
        to.value = value
    }

    /**
     * Конвертирует число из одной валюты в другую валюту
     * @param value {String} - конвертируемое число
     * @param from {String} - код валюты из которой конвертируем
     * @param to {String} - код валюты в которую конвертируем
     * @param reverse {Boolean} - направление конвертации. По умолчанию из левой (основной) в правую (второстепенную). true - из второстепенной валюты в основную
     */
    async function convert(value:string,from?:string, to?:string,reverse:boolean = false):Promise<string | undefined> {
        if(!value || !from || !to) return '0'
        let pairFrom:CurrencyPair = defaultCurrencyPair
        let pairTo:CurrencyPair = defaultCurrencyPair
        const currencyPairs = await getPairs()
        currencyPairs.forEach((p:any) => {
            if (p.quote_currency === from) {
                pairFrom = p
            }
            if (p.quote_currency === to) {
                pairTo = p
            }
        })
        let ratesFrom = defaultPairRate
        let ratesTo = defaultPairRate
        const exchangeRates = await getRates()
        exchangeRates.forEach((k:any) => {
            if (k.pair === `${pairFrom.base_currency}/${pairFrom.quote_currency}`) {
                ratesFrom = k
            }
            if (k.pair === `${pairTo.base_currency}/${pairTo.quote_currency}`) {
                ratesTo = k
            }
        })
        const baseFrom = +value / parseFloat(ratesFrom.rate)
        const baseTo = baseFrom * parseFloat(ratesTo.rate)

        const baseFromReversed = +value / parseFloat(ratesTo.rate)
        const baseToReversed = baseFromReversed * parseFloat(ratesFrom.rate)

        hasConverted.value = true
        return !reverse ? baseTo.toFixed(2) : baseToReversed.toFixed(2)
    }

    /**
     * Рассчитывает % комиссии
     */
    const getCommission = async () => {
        let pairFrom:CurrencyPair = defaultCurrencyPair
        let pairTo:CurrencyPair = defaultCurrencyPair
        const currencyPairs = await getPairs()
        currencyPairs.forEach((p:any) => {
            if (p.quote_currency === from.value) {
                pairFrom = p
            }
            if (p.quote_currency === to.value) {
                pairTo = p
            }
        })
        const commission = pairFrom.commission + pairTo.commission
        convertationCommission.value = commission.toString()
        return convertationCommission.value
    }

    return {
        convertationCommission,
        hasConverted,
        from,
        to,
        setFrom,
        setTo,
        pairs,
        getPairs,
        rates,
        getRates,
        convert,
        getCommission
    }
})
